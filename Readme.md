Examples of using OTF with docker-compose and traefik

Prerequisities:
* docker
* docker-compose 
* python 3.x (for generating jwt acces tokens)

Traefik basic sample doc : https://doc.traefik.io/traefik/user-guides/docker-compose/basic-example/

Doc of OTF: https://opentestfactory.gitlab.io/orchestrator/index.html


## Launch a project 

(for ex. `traefik-orchestrator-http`):

You can either provide your trusted key(s) or let the orchestrator create a JWT token for you. (It is not recommended to use this created JWT token in a production environment.)

To provide keys and token : 
```sh
cd traefik-orchestrator-http/data
./generate_keys.sh
python3 -m pip install PyJWT[crypto]
python3 generate_token.py
```
Before running the project:
```sh
cd traefik-orchestrator-http
touch .env
```
use `env_example` to fill in the variables in `.env`

check docker-compose.yml and (optional) uncomment configurations you need (volumes, variables, containers)
 
To run the project:
```sh
docker-compose up -d
```
