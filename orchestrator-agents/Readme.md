## Launch a project 


1. Install [Docker](https://docs.docker.com/engine/installation/) ([Engine](https://docs.docker.com/engine/installation/), [Compose](https://docs.docker.com/compose/install/))


2. Either create and provide JWT token or let the orchestrator create it for you.


    <details><summary>     Click here to check the instructions how to create tokens </summary>
    

    ```sh
          cd orchestrator-agents/data
          ./generate_keys.sh
          python3 -m pip install PyJWT[crypto]
          python3 generate_token.py
    ```

    </details>
    Or browse the logs of orchestrator after it's up


3. Fill in variables in `docker-compose.yml` or in `.env` (see examples in `env_example`)
    ```sh
    cd orchestrator-agents
    touch .env  
    ```

    check `docker-compose.yml` and (optional) uncomment configurations you need (volumes, variables, containers)

4. Deploy orchestrator using `docker-compose` within the same folder in daemon mode ('-d' argument)

    ```sh
    $ docker-compose up -d
    ```



5. Orchestrator is ready on IP address of deployed environment at ports

    ```
    $ http://IP_ADDRESS:7774
    $ http://IP_ADDRESS:7775
    $ http://IP_ADDRESS:7776    
    $ http://IP_ADDRESS:24368
    $ http://IP_ADDRESS:38368  
    ```
5. If you didn't provide certificate, you can check the token generated for you in logs of the orchestrator :
    ```sh
    docker logs orchestrator 2>&1 | grep --after-context=10 "Creating temporary JWT token"
    ```  
 
