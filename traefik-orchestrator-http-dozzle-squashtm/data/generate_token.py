import jwt  # pip install PyJWT[crypto]

with open('trusted_key.pem', 'r') as f: pem = f.read()
with open('trusted_key.pub', 'r') as f: pub= f.read()

# create a signed token
token = jwt.encode({'iss': 'you', 'sub': 'me'}, pem, algorithm='RS512')
print(token)

# verify it
payload = jwt.decode(token, pub, algorithms=['RS512'])
print(payload)