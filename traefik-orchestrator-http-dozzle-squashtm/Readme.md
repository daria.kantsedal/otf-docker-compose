## Launch a project 


1. Install [Docker](https://docs.docker.com/engine/installation/) ([Engine](https://docs.docker.com/engine/installation/), [Compose](https://docs.docker.com/compose/install/))


2. Either create and provide JWT token or let the orchestrator create it for you.


    <details><summary>     Click here to check the instructions how to create tokens </summary>
    

    ```sh
          cd traefik-orchestrator-http/data
          ./generate_keys.sh
          python3 -m pip install PyJWT[crypto]
          python3 generate_token.py
    ```

    </details>
    Or browse the logs of orchestrator after it's up


3. Fill in variables in `docker-compose.yml` or in `.env` (see examples in `env_example`)
    ```sh
    cd traefik-orchestrator-http-dozzle-squashtm
    touch .env  
    ```

    check `docker-compose.yml` and (optional) uncomment configurations you need (volumes, variables, containers)

4. Deploy orchestrator using `docker-compose` within the same folder in daemon mode ('-d' argument)

    ```sh
    $ docker-compose up -d
    ```



5. SquashTM is ready on IP address of deployed environment and port 80

    ```
    $ http://IP_ADDRESS:80/squash

    ```

    Orchestrator is ready on its endpoints accessed by the same port: 

    
    ```
    $ http://IP_ADDRESS:80/workflows

    ```

    You can check their logs on the same IP address on context `/logs`

    ```
    $ http://IP_ADDRESS:80/logs

    ```

5. If you didn't provide certificate, you can check the token generated for you in logs of the orchestrator

 
